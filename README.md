# xopoly default application

This project is the default application for [xopoly](https://www.npmjs.com/package/xopoly).

## Installation

``` bash
  $ npm install -g xopoly-app
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
