var xopoly = require('xopoly');
var boardDefault = require('xopoly-board-default');

module.exports = {
    start: function() {

        // Load application boards
        //xopoly.addBoards(path.resolve(__dirname + '/../boards'));

        // Load module boards
        xopoly.addBoards(boardDefault.getPath());

        // Begin
        xopoly.start();
    }
};
